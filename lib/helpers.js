//global sub object
_subManager = {};

_G = {
  //if user is logged out
  loggedOut: function () {
    return !Meteor.userId() || Meteor.loggingIn();
  }
}