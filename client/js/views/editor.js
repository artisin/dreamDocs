var globalSnap = new ReactiveVar(null);
_subManager.files = _subManager.files || new SubsManager();
_subManager.users = _subManager.users || new SubsManager();
_subManager.commits = _subManager.commits || new SubsManager();
var usersSub = _subManager.users;
var filesSub = _subManager.files;
var commitsSub = _subManager.commits;


Template.editor.onCreated(function () {
  var self = this;
  Session.set('document', null);
  Session.set('docPreview', true);
  this.ready = new ReactiveVar(false);
  this.isMarkdown = new ReactiveVar(true);
  // this.isMarkdownPrv = new ReactiveVar(true);
  this.file = new ReactiveVar(false);
  //snapshort
  this.snapshot = new ReactiveVar({});
  //editor instacne
  // this.editor = 

  var ref = Meteor.user().profile;
  //sub
  self.autorun(function () {
    var filesHandle  = filesSub.subscribe('files', ref.repo, ref.repoBranch),
        usersHandle  = usersSub.subscribe('users'),
        commitHandle = commitsSub.subscribe('commits', ref.repo, ref.repoBranch);

    //to cycle
    var subs = [filesHandle, usersHandle, commitHandle];
    //cycle through subs
    var ready = _.every(_.map(subs, function (val) {
      return val.ready();
    }), _.identity);
    //set ready var
    if (ready) {
      self.ready.set(true);
    }
  });

  //watch doc change and reset snap
  self.autorun(function () {
    if (Session.get('document')) {
      globalSnap.set(null);
    }
  });
});

Template.editor.onRendered(function () {
  //watch selected doc
  var self = this;
  Tracker.autorun(function () {
    var file = Files.findOne(Session.get('document'));
    if (file) {
      self.file.set(file);
    }
  });
});

Template.editor.helpers({
  subReady: function () {
    return Template.instance().ready.get();
  },
  //passes ref to editorPrv tmeplate
  getFile: function () {
    // debugger
    return Session.get('document')
           ? Files.findOne(Session.get('document'))
           : false;
  },
  docModePrv: function () {
    return Session.get('docPreview');
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Editor Nav
/*----------------------------------------------------------------------------*/
Template.editorNav.helpers({
  docTitle: function () {
    var doc = Template.instance().props.doc();
    return doc ? doc.title : 'Please Select A Doc';
  },
  canEditClass: function () {
    return Session.get('document') ? '' : ' disabled';
  },
  docMode: function () {
    return Session.get('docPreview') ? 'Edit' : 'Preview';
  },
});
/*-----------------------------*/
/// Events
/*-----------------------------*/
Template.editorNav.events({
  //change mode
  'click .docMode': function (e) {
    e.preventDefault();
    var cur = Session.get('docPreview');
    Session.set('docPreview', !cur);
    //get shareJs
    if (cur) {
      var id = Session.get('document');
      Meteor.call('getShareJS', {_id: id});
    } else {
      //backup catch
      // debugger
      var snap = globalSnap.get();
      // debugger
      if (snap) {
        //update cache
        Meteor.call('updateCache', {
          _id: Session.get('document'),
          snap: globalSnap.get()
        });
      }
    }
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Editor Prv
/*----------------------------------------------------------------------------*/
Template.editorPrv.helpers({
  getDoc: function () {
    return Template.instance().props.doc();
  },
  displayDoc: function () {
    // debugger
    var doc = Template.instance().props.doc();
    var markdown = /\.(md)$/i;

    //if markdown phrase and displat
    if (markdown.test(doc.title)) {
      Session.set('docPreview', true);
      var reader = new CommonMark.Parser();
      var writer = new CommonMark.HtmlRenderer();
      var content = globalSnap.get() || (doc.cache || doc.content);
      // console.log(content)
      if (content.length) {
        var parsed = reader.parse(content);
        var display = writer.render(parsed);
        return display;
      }
      return '<h1>Not Shit To Display.</h1>';
    }
    return '<h1>Sorry No Preview Avalible For This File</h1>';
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Edit Mode
/*----------------------------------------------------------------------------*/
Template.editorEdit.onCreated(function () {
  // debugger
  var doc = Template.instance().props.doc();
  //editor ref
  this.editor = new ReactiveVar(null);


  Meteor.call('getShareJS', doc, function (err, res) {
    console.log(arguments)
    // debugger
    if (doc.cache !== res.snapshot) {
      console.log('update cache')
      // Meteor.call('updateCache', {_id: id, snap: res.snapshot});
    }
  });
})

Template.editorEdit.helpers({
  docId: function () {
    return Session.get('document');
  },
  isImage: function () {
    var doc = Template.instance().props.doc();
    if (doc) {
      var image = /\.(gif|jpg|jpeg|tiff|png|bmp)$/i;
      if (image.test(doc.title)) {
        if (doc.type !== 'image') {
          Meteor.call('setFileType', doc, 'image');
        }
        return true;
      }
      //no Image
      if (doc.type !== 'file') {
        Meteor.call('setFileType', doc, 'file');
      }
      return false;
    }
  },
  //configs 
  config: function() { // set default theme and autocomplete
    var tmpl = Template.instance();
    return function(editor) {
      // debugger
      editor.setTheme('ace/theme/chrome');
      editor.setShowPrintMargin(false);
      editor.getSession().setUseWrapMode(true);
      editor.$blockScrolling = Infinity;
      // var beautify = editor.require('ace/ext/beautify');
      // editor.commands.addCommands(beautify.commands);
      editor.setOptions({
        vScrollBarAlwaysVisible: true,
        highlightGutterLine: true,
        animatedScroll: true
      });
      //stor ref
      tmpl.editor.set(editor);
    };
  },
  setMode: function() { // different style on filetype
    var doc = Template.instance().props.doc();
    return function(editor) {
      // debugger
      var modelist = ace.require('ace/ext/modelist');
      if (doc) {
        var mode = modelist.getModeForPath(doc.title);
        editor.getSession().setMode(mode.mode);
        return editor;
      }
    };
  }
});

Template.editorEdit.events({
  //stores global snap
  'keyup #editor': function(e) {
    e.preventDefault();
    // debugger
    globalSnap.set(Template.instance().editor.get().getValue());
  }
})


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Image
/*----------------------------------------------------------------------------*/
Template.renderImage.helpers({
  html: function() { // return link to file on github
    debugger
    // var file = Files.findOne(Session.get('document'));
    if (file) {
      return file.html;
    }
  },
  raw: function() { // return link to github image
    // var file = Files.findOne(Session.get('document'));
    if (file){
      return file.raw;
    }
  }
});