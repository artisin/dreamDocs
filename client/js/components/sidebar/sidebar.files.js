/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebarFiles onCreated
/*----------------------------------------------------------------------------*/
Template.sidebarFiles.onCreated(function () {
  var self = this;
  this.ready = new ReactiveVar(false);
  //sub
  self.autorun(function () {
    //set ready var
    if (_subManager.files && _subManager.files.ready()) {
      self.ready.set(true);
    } else {
      _subManager.files = _subManager.files || new SubsManager();
      // var filesSub = _subManager.files;
    }
  });
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebarFiles onRendered
/*----------------------------------------------------------------------------*/
Template.sidebarFiles.onRendered(function () {});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebarFiles Helpers
/*----------------------------------------------------------------------------*/
Template.sidebarFiles.helpers({
  subReady: function () {
    return Template.instance().ready.get();
  },
  files: function () {
    return Files.find({}, {sort: {'title': 1}} ).map(function (file) {
      return file;
    });
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebarFiles Events
/*----------------------------------------------------------------------------*/
Template.sidebarFiles.events({
  //Change doc
  'click .fileLabel': function (e) {
    // debugger
    e.preventDefault();
    var self = this;
    Session.set('document', this._id);

    // Session.set('owner', this.userId);
    // Session.set('isOwner', this.userId === Meteor.userId());
    // Session.set('invitedUsers', this.invitedUsers);
    
    return Meteor.call('postShareJS', self);
  },

  //@todo New?
  'click .NEW': function (e) {
    e.preventDefault();
    debugger

    var userId = Meteor.userId();
    if (!Session.equals('document', this._id)) {
      //sets new file doc
      return Documents.insert({
        title: 'untitled',
        userId: userId,
        invitedUsers: [],
        private: false,
        revokePending: []
      }, function(err, id) {
        if (!id) {
          return;
        }
        Session.set('document', id);
        Session.set('owner', userId);
        Session.set('isOwner', true);
        Session.set('document', this._id);
        // Session.set('invitedUsers', []);
        return Meteor.call('postShareJS', this);
      });
    }
  }
});