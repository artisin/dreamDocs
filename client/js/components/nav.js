Template.nav.helpers({
  userLoggedIn: function () {
    return ! Meteor.userId() || Meteor.loggingIn();
  }
});



/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Accounts
/*----------------------------------------------------------------------------*/
/*-----------------------------*/
/// Nav logging in funk
/*-----------------------------*/
Template.userLoggedin.events({
  //account
  'click .toHome': function(e) {
    e.preventDefault();
    console.log('toHome')
    FlowRouter.go('/home');
  },
  'click .toEditor': function(e) {
    e.preventDefault();
    Session.set('document', null);
    FlowRouter.go('/editor');
  },
  'click .toAccount': function(e) {
    e.preventDefault();
    FlowRouter.go('/account');
  }
});




/*-----------------------------*/
/// Login
/*-----------------------------*/
Template.userLoggedout.events({
  'click .login': function(e) {
    Meteor.loginWithGithub({
      requestPermissions: ['user', 'public_repo'],
      loginStyle: 'redirect'
    }, function(err) {
      if (err) {
        console.log(err);
        // Session.set('errorMessage', err.reason);
      }
    });
  }
});
/*-----------------------------*/
/// Logout
/*-----------------------------*/
Template.userLoggedin.helpers({
  name: function () {
    return Meteor.user().profile.name;
  }
});
