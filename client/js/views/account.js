_subManager.repos = _subManager.repos || new SubsManager();
var reposSub = _subManager.repos;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// account onCreated
/*----------------------------------------------------------------------------*/
Template.account.onCreated(function () {
  var self = this;
  self.ready = new ReactiveVar(false);

  self.autorun(function () {
    var repoHandle = reposSub.subscribe('repos');
    var subs = [repoHandle];

    //cycle through subs
    var ready = _.every(_.map(subs, function (val) {
      return val.ready();
    }), _.identity);

    //set ready var
    if (ready) {
      self.ready.set(true);
    }
  });
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// account onRendered
/*----------------------------------------------------------------------------*/
Template.account.onRendered(function () {});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// account Helpers
/*----------------------------------------------------------------------------*/
Template.account.helpers({
  subsLoaded: function () {
    var ready = Template.instance().ready.get();
    if (Repos.find({}).count() === 0) {
      Meteor.call('getAllRepos');
    }
    return ready;
  },
  semanticInit: function () {
    Meteor.setTimeout(function () {
      $('.dropdown').dropdown();
    }, 100);
  },
  repos: function() {
    return Repos.find({}, {sort: {'repo.owner': -1, 'repo.name': 1}} ).fetch();
  },
  currentRepo: function () {
    if (!Session.get('document')) {
      return 'You have not selected a repo yet';
    }
    return Meteor.user().profile.repoName;
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// account Events
/*----------------------------------------------------------------------------*/
Template.account.events({
  'click .loadGHData': function(e) { // load in repos from github
    e.preventDefault();
    Meteor.call('getAllRepos');
  },
  'click .repoSelector': function(e) { // show the available repos
    e.preventDefault();
    //null
  },
  'click .repoSelect': function(e) { //click repo
    e.preventDefault();
    Session.set('dimmer', true);
    Meteor.call('loadRepo', this, function (err, res) {
      if (err) {
        //@todo error
        console.log(err);
      } else {
        Session.set('dimmer', false);
      }
    });

  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// account onDestroyed
/*----------------------------------------------------------------------------*/
Template.account.onDestroyed(function () {});