/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// editorSplash onCreated
/*----------------------------------------------------------------------------*/
Template.editorSplash.onCreated(function () {
  var self = this;
  var fileSub = _subManager.files;
  var commitSub = _subManager.commits;
  this.ready = new ReactiveVar(false);
  self.autorun(function () {
    if (fileSub.ready() && commitSub.ready()) {
      self.ready.set(true);
    }
  });
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// editorSplash Helpers
/*----------------------------------------------------------------------------*/
Template.editorSplash.helpers({
  ready: function () {
    return Template.instance().ready.get();
  },
  files: function () {
    return Files.find({});
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// editorSplash Events
/*----------------------------------------------------------------------------*/
Template.editorSplash.events({
  'click .file': function (e) {
    e.preventDefault();
    var self = this;
    // Session.set('document', this._id);
    // debugger
    var path = FlowRouter.path('editorRoute', {
      file: self.title,
      id: self._id,
      branch: self.branch
    });
    FlowRouter.go(path);
  }
});


