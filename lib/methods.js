// common (server and client) methods


Meteor.methods({

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Feed Managment
/*----------------------------------------------------------------------------*/
  addMessage: function (msg) { // add a generic message to the activity feed
    if (msg.length) {
      Messages.insert({
        owner: Meteor.userId(),
        repo: Meteor.user().profile.repo,
        name: Meteor.user().profile.login,
        message: msg,
        time: Date.now()
      });
    } else {
      throw new Meteor.Error('null-message'); // passed in empty message
    }
  },

  addUserMessage: function (usr, msg) { // add message, with userId() (issues)
    var poster = Meteor.users.findOne(usr);
    if (msg.value !== '') {
      if (poster) {
        Messages.insert({
          owner: poster._id,
          repo: poster.profile.repo,
          name: poster.profile.login,
          message: msg,
          time: Date.now()
        });
      } else
        throw new Meteor.Error('null-poster'); // user account is not in mongo
    } else
      throw new Meteor.Error('null-message'); // they passed in empty message
  },

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Task Managment
/*----------------------------------------------------------------------------*/
  addTask: function (text) { // add a task to repo, with this userid
    Tasks.insert({
      text: text,
      time: new Date(),
      owner: Meteor.userId(),
      repo: Meteor.user().profile.repo,
      username: Meteor.user().profile.login
    });
    Meteor.call('addMessage', 'created task - ' + text); // post to the feed
  },

  setChecked: function (task) { // on task check/uncheck, notify
    Tasks.update(task._id, { $set: { checked: ! task.checked } });
    var act = (task.checked ? 'revived' : 'completed');
    Meteor.call('addMessage', act + ' task  - ' + task.text);
  },

  deleteTask: function (task) { // delete a task from the current repo
    Tasks.remove(task._id); // actually remove it
    Meteor.call('addMessage', 'deleted task - ' + task.text);
  },

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Repo Managment
/*----------------------------------------------------------------------------*/
  loadRepo: function(gr) { // load a repo into code pilot
    Meteor.call('setRepo', gr); // set the active project / repo
    // get all the possible branches
    Meteor.call('initBranches', gr);
    Meteor.call('initTags', gr);

    //set meta ref data
    Meteor.call('setMeta', gr);
    // load the head of gr branch into CP
    Meteor.call('loadBranch', gr);
    var full = gr.repo.owner.login + '/' + gr.repo.name;
    Meteor.call('addMessage', 'started working on repo - ' + full);
    Meteor.call('postAllShareJS');
  },

  setRepo: function(gr) { // set git repo & default branch
    return Meteor.users.update({
      _id: Meteor.userId()
    }, {
      $set: {
        'profile.repo': gr._id,
        'profile.repoName': gr.repo.name,
        'profile.repoOwner': gr.repo.owner.login,
        'profile.repoBranch': gr.repo.owner.default_branch
      }
    });
  },

  forkRepo: function(user, repo) { // create a fork of a repo for user
    try { // that is, if the repo exists/isForkable
      Meteor.call('getRepo', user, repo);
      Meteor.call('postRepo', user, repo);
      Meteor.call('getAllRepos');
    } catch (err) {
      throw new Meteor.Error('null-repo'); // this repo no fork :O
      dlog(err);
    }
  },

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Tag Managment
/*----------------------------------------------------------------------------*/
  initTags: function (gr) {
    var tags = Meteor.call('getTags', gr);
    Repos.update(gr._id, { $set: {tags: tags }});
  },


  setMeta: function (gr) {
    var br = gr.repo.default_branch;
    var sha = Repos.findOne(gr._id).branches;
    sha = _.map(sha, function (val) {
      if (val.name === br) {
        return val.commit.sha;
      }
    });
    var updateDb = function (rel, cur) {
      Meteor.users.update({
          _id: Meteor.userId()
        }, {
          $set: {
            'profile.repoRelease': cur,
            'profile.repoBranch': br,
            'profile.sha': sha[0]
          }
      });
      Repos.update(gr._id, {
        $set: {
          realease: rel
        }
      });
    };
    //get current realse
    Meteor.call('getCurRelease', gr, function (err, res) {
      if (err) {
        logger.warn(err);
      }else if (res) {
        if (res.length) {
          res = res[0];
          updateDb(res, res.tag_name);
        } else {
          updateDb('None', {});
        }
      }
    });
  },
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Branch Managment
/*----------------------------------------------------------------------------*/
  // for the current repo, just overwrite branches with new
  initBranches: function(gr) { // get all branches for this repo
    var brs = Meteor.call('getBranches', gr); // res from github
    Repos.update(gr._id, { $set: {branches: brs }});
  },

  addBranch: function(bn) { // create a new branch from branchname (bn)
    var repo = Repos.findOne(Meteor.user().profile.repo);
    var branch = Meteor.user().profile.repoBranch;
    var parent = Meteor.call('getBranch', branch).commit.sha;
    var newBranch = Meteor.call('postBranch', bn, parent);
    Meteor.call('initBranches', repo);
    Meteor.call('addMessage', 'created branch - ' + bn);
  },

  loadBranch: function(gr) { // load a repo into code pilot
    var bn = gr.repo.default_branch;
    Meteor.call('initCommits'); // pull commit history for this repo
    Meteor.call('loadHead', bn); // load the head of this branch into CP
    Meteor.call('addMessage', 'started working on branch - ' + bn);
  }
});
