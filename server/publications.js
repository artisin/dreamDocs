var logPub = function (name, args) {
  args = args || {args: false};
  logger.info('[publish -> ' + name + ']', args);
};

Meteor.publish('repos', function() { // only serve writable repos
  logPub('repos');
  return Repos.find({users: this.userId});
});
Meteor.publish('commits', function(repoId, branch) { // serve b+r commits
  logPub('commits', {repoId: repoId, branch: branch});
  return Commits.find({repo: repoId, branch: branch});
});
Meteor.publish('files', function(repoId, branch) { // only serve b+r files
  logPub('files', {repoId: repoId, branch: branch});
  return Files.find({repo: repoId, branch: branch});
});
Meteor.publish('messages', function(repoId) { // only serve repo msgs
  logPub('messages', {repoId: repoId});
  return Messages.find({repo: repoId});
});

//@todo user gruoups
Meteor.publish('users', function() {
  logPub('users');
  return Meteor.users.find({});
});

// Meteor.publish('tasks', function(repoId) { // only serve repo tasks
//   return Tasks.find({repo: repoId});
// });
// Meteor.publish('issues', function(repoId) { // only serve repo issues
//   return Issues.find({repo: repoId});
// });
// Meteor.publish('screens', function() { // serve all issue screenshots
//   return Screens.find({});
// });

// Meteor.publish('documents', function() {
//   // var users;
//   var users = [this.userId];
//   return Documents.find({
//     $or: [
//       {
//         userId: users[0]
//       }, {
//         invitedUsers: {
//           $in: users
//         }
//       }
//     ]
//   });
// });

// //@todo account limited with roles
// Meteor.publish('users', function() {
//   return Meteor.users.find({});
// });