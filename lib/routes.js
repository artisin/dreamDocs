
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Home
/*----------------------------------------------------------------------------*/
FlowRouter.triggers.enter([function (context) {
  var status = _G.loggedOut();
  if (status) {
    console.warn('Logged Out');
    FlowRouter.go('/login');
  } else if (!status && context.path === '/login') {
    FlowRouter.go('/');
  }
}], {except: ['home']});


//Home
FlowRouter.route('/', {
  name: 'home',
  action: function() {
    BlazeLayout.render('App', {
      top: 'nav',
      side: 'sidebar',
      main: 'home',
      sidebarData: 'sideFeed'
    });
  }
});

FlowRouter.route('/home', {
  triggersEnter: [function () {
    if (!Session.get('document') && Meteor.user()) {
      var cur = Meteor.user().profile.repo;
      if (cur) {
        Session.set('repo', cur);
      }else {
        console.log('Need to set up repo');
      }
    }
  }],
  action: function() {
    BlazeLayout.render('App', {
      top: 'nav',
      side: 'sidebar',
      main: 'home',
      sidebarData: 'sideFeed'

    });
  }
});


//Login
FlowRouter.route('/login', {
  action: function() {
    BlazeLayout.render('App', {
      top: 'nav',
      main: 'login'
    });
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Editor
/*----------------------------------------------------------------------------*/
FlowRouter.route('/editor', {
  action: function() {
    BlazeLayout.render('App', {
      top: 'nav',
      side: 'sidebar',
      main: 'editor',
      sidebarData: 'sidebarFiles'
    });
  }
});

FlowRouter.route('/editor/:file/:branch/:id', {
  name: 'editorRoute',
  triggersEnter: [function(params) {
    //checks to see if we need to sub first
    params = params.params;
    var createSub = !_subManager.files;
    _subManager.files = _subManager.files || new SubsManager();
    var filesSub = _subManager.files;
    //wait for sub if needed
    Meteor.autorun(function () {
      var handle = filesSub;
      if (createSub) {
        handle = filesSub.subscribe('files', params.id, params.branch)
      }
      //wait for sub
      if (handle.ready()) {
        var file = Files.findOne(params.id);
        Meteor.call('postShareJS', file);
        Session.set('document', params.id);
      }
    });
  }],
  action: function() {
    BlazeLayout.render('App', {
      top: 'nav',
      side: 'sidebar',
      main: 'editor',
      sidebarData: 'sidebarFiles'
    });
  }
});

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Account
/*----------------------------------------------------------------------------*/
FlowRouter.route('/account', {
  action: function() {
    BlazeLayout.render('App', {
      top: 'nav',
      side: 'sidebar',
      main: 'account',
      sidebarData: 'sidebarFeed'
    });
  }
});