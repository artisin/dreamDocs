
/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebar onCreated
/*----------------------------------------------------------------------------*/
Template.sidebar.onCreated(function () {
  this.sidebarOpen = new ReactiveVar(false);
});

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebar onRendered
/*----------------------------------------------------------------------------*/
Template.sidebar.onRendered(function () {});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebar Helpers
/*----------------------------------------------------------------------------*/
Template.sidebar.helpers({
  sidebarClosed: function () {
    return !Template.instance().sidebarOpen.get();
  }
});


/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// sidebar Events
/*----------------------------------------------------------------------------*/
Template.sidebar.events({
  'click .closeSidebar': function (e) {
    e.preventDefault();
    var tmpl = Template.instance();
    var tm = new TimelineMax();
    var animComplete = function () {
      tmpl.sidebarOpen.set(false);
      Meteor.setTimeout(function () {
        TweenMax.to('.openSidebar', 0.15, {autoAlpha: 1});
      }, 100);
    };
    //move sidebar off screen
    tm.to('.sidebar_wrapper', 0.25, {x: '-100%', z: '0.01'})
      .to('.main_wrapper', 0.25, {
        left: '0%',
        z: '0.01',
        onComplete: animComplete
      }, '-=0.25');
  },
  'click .openSidebar': function (e) {
    e.preventDefault();
    var tmpl = Template.instance();
    var tm = new TimelineMax();
    var animComplete = function () {
      tmpl.sidebarOpen.set(true);
    };
    //move sidebar off screen
    tm.to('.openSidebar', 0.15, {opacity: 0})
      .to('.main_wrapper', 0.25, {left: '15%', z: '0.01'})
      .to('.sidebar_wrapper', 0.25, {
        x: '0%',
        z: '0.01',
        onComplete: animComplete
      }, '-=0.25');
  },
  //logout
  'click .logout': function(e) {
    e.preventDefault();
    Meteor.logout(function(err) {
      if (err) {
        console.log(err);
      }
    });
  }
});

