
Docs = new Mongo.Collection('docs'); // used inside sharejs (sjs)

/*  _id - unique identifier, corresponds to File._id
    data.v - latest version of file in the editor
    data.snapshot - latest content of file in the editor
    NOTE - some fields omitted
    */

Files = new Mongo.Collection('files'); // used with github

/*  _id - unique identifier, same as Doc._id
    repo - unique identifier of repo file belongs to
    branch - name of the branch it is from
    title - name of the file
    cache - latest version of commit, for diff
    content - live content of the file
    type - file, nullmode, or image
    html - link to file page on github
    raw - link to raw file content (github)
    */

Messages = new Mongo.Collection('messages'); // client side feed

/*  _id - unique identifier of message
    repo - unique identifier of repo file belongs to
    name - login name of message creator
    message - feed item content
    time - message creation time
    */

// Tasks = new Mongo.Collection('tasks');

/*  _id - unique identifier of task
    text - name of the task, displayed
    repo - unique identifier of repo task belongs to
    time - time of creation of task
    owner - userid of creator
    username - profile.login of creator
    checked - boolean whether done or not
    */

// Issues = new Mongo.Collection('issues');

/*  _id - unique identifier of task
    repo - unique identifier of issue ask belongs to
    id - github assigned id issue, also unique
    issue - response from server
    screen - id of the screenshot
    feedback - param from feedback.js
    */


Commits = new Mongo.Collection('commits');

/*  _id - unique identifier of commit
    repo - unique identifier of repo file belongs to
    commit - blob from git// TODO: TRIM THIS DOCUMENT!!!
    */

Repos = new Mongo.Collection('repos'); // PROJECT ID

/*  _id - unique identifier of commit
    sha - git hash code for this commit
    users - array of user ids that can push
    branches - array of branches (see below)
    repo - unique identifier of repo file belongs to
    */

// Branch (inside)
/*  _id - unique identifier of commit
    repo - unique identifier of repo file belongs to
    sha - git hash code for this commit
    */
   


// this.Documents = new Meteor.Collection('documents');

// this.Documents.allow({
//   insert: function(userId, doc) {
//     return !!userId;
//   },
//   update: function(userId, doc, fields) {
//     var ref = fields[0] === 'invitedUsers' || ref === 'title' || ref === 'revokePending';
//     return doc.userId === userId && ref;
//   },
//   remove: function(userId, doc) {
//     return doc.userId === userId;
//   }
// });

// Meteor.methods({
//   deleteDocument: function(id) {
//     Documents.remove(id);
//     //ignore errors
//     if (!this.isSimulation) {
//       return ShareJS.model['delete'](id);
//     }
//   }
// });

// // Observe changes on documents, especially revokePending array property
// if (Meteor.isClient) {
//   var cursor = this.Documents.find();
//   var indexOf = [].indexOf || function(item) {
//     for (var i = 0, l = this.length; i < l; i++) {
//       if (i in this && this[i] === item) { return i; }
//     }
//     return -1;
//   };
//   var handle = cursor.observe({
//     changed: function(doc) {
//       var userId;
//       userId = Meteor.userId();
//       Session.set('invitedUsers', doc.invitedUsers);
//       if (doc.userId === userId) {
//         return;
//       }
//       if ((Session.get('document') === doc._id) && (indexOf.call(doc.revokePending, userId) >= 0)) {
//         Session.set('document', null);
//         Session.set('owner', "");
//         Session.set('invitedUsers', []);
//         console.warn('display modal');
//         // return $('#revokedModal').trigger('revokedEvent');
//       }
//     }
//   });
// }

// // Observe changes on documents, especially revokePending array property
// // Gracefully withdraw invitations, and clear out revokePending array
// if (Meteor.isServer) {
//   // debugger
//   var cursor = this.Documents.find();
//   //watch the cursor for changes
//   var handle = cursor.observe({
//     changed: function(doc) {
//       if (!(doc.revokePending.length > 0)) {
//         return;
//       }
//       //Give 500 ms for clients to clear out their
//       //session variables before kicking them out
//       return Meteor.setTimeout((function() {
//         Documents.update({
//           _id: doc._id
//         }, {
//           $pullAll: {
//             invitedUsers: doc.revokePending
//           }
//         });
//         return Documents.update({
//           _id: doc._id
//         }, {
//           $pullAll: {
//             revokePending: doc.revokePending
//           }
//         });
//       }), 500);
//     }
//   });
// }