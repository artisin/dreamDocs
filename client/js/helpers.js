/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Function
/*----------------------------------------------------------------------------*/
var userIdsByEmail = function(emails) {
  var result, users;
  result = [];
  users = Meteor.users.find({
    "emails.0.address": {
      $in: emails
    }
  }).fetch();
  users.forEach(function(user) {
    return result.push(user._id);
  });
  return result;
};

this.UserEmailsById = function(ids) {
  var result, users;
  result = [];
  users = Meteor.users.find({
    "_id": {
      $in: ids
    }
  }).fetch();
  users.forEach(function(user) {
    return result.push(user.emails[0].address);
  });
  return result;
};

this.UserEmailById = function(id) {
  var user;
  if (!id) {
    return "";
  }
  user = Meteor.users.findOne({
    _id: id
  });
  return user.emails[0].address;
};

this.SendInviteToUsers = function(id, userEmails) {
  if (userEmails.length > 0) {
    return Documents.update({
      _id: id
    }, {
      $addToSet: {
        invitedUsers: {
          $each: userIdsByEmail(userEmails)
        }
      }
    });
  }
};

this.RevokeInviteFromUsers = function(id, userEmails) {
  if (userEmails.length > 0) {
    return Documents.update({
      _id: id
    }, {
      $addToSet: {
        revokePending: {
          $each: userIdsByEmail(userEmails)
        }
      }
    });
  }
};

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
// Template
/*----------------------------------------------------------------------------*/
Template.registerHelper("withif", function(obj, options) {
  if (obj) {
    return options.fn(obj);
  } else {
    return options.inverse(this);
  }
});

Template.registerHelper('isOwner', function() {
  return Session.get("isOwner");
});

Template.registerHelper('owner', function() {
  return UserEmailById(Session.get("owner"));
});

Template.registerHelper('time', function(time) {
  return moment.unix(time).format('MMM, Do, h:mA');
});

