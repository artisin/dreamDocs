
var inDevelopment = function() {
  return process.env.NODE_ENV === 'development';
};


Meteor.startup(function () { // get correct github auth key

  ServiceConfiguration.configurations.remove({service: 'github'});
  var prodAuth = JSON.parse(Assets.getText('production.json'));
  var devAuth = JSON.parse(Assets.getText('development.json'));
  var GHAuth = ( inDevelopment() ? devAuth : prodAuth );
  ServiceConfiguration.configurations.insert( GHAuth );

  // node-github setup
  github = new GitHub({
    version: '3.0.0',
    timeout: 5000,
    debug: true, // boolean declared above
    protocol: 'https',
    headers: { 'User-Agent': 'Dream Docs' }
  });

  // oauth for api 5000/hour
  github.authenticate({
    type: 'oauth',
    key: GHAuth.clientId,
    secret: GHAuth.secret
  });

  // Global.github = github;

});


/*-----------------------------*/
/// Logging
/*-----------------------------*/
var consoleOptions = {
  colorize: true,
  levels : {debug: 0, info : 1, warn: 2, error: 3},
  colors : {debug: 'blue', info : 'green', warn: 'orange', error: 'red'},
  handleExeptions: true,
  humanReadableUnhandledException: true
};

// Add & configure the console transport 
logger.addTransport('console', consoleOptions);